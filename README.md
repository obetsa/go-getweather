### Infrastructure with AWS Cloud 
![alt text](https://github.com/obetsa/go-getweather/blob/main/go-getweather.png?raw=true)

### Application (CI/CD)
## Terraform
terraform init
terraform apply
## Kubernetes
kubectl apply -f .

### Application (Coding)
## go-getweather  -  simple weather application for getting your current forecast
- Golang / Gin
- Java Script / Axios
- Bootstrap 5
- API by www.weatherapi.com

## Docker
sudo docker run -d --restart always -p 8080:8080 obetsa/go-getweather:latest